export class User {
    id?: string
    username: string
    email: string
    image?: string
    active?: boolean
    activeAgo?: number

    constructor(
        id: string = '', username: string = '', email: string = '', image: string = '', active: boolean = false, activeAgo: number = 0
    ) {
        this.id = id
        this.username = username
        this.email = email
        this.image = image
        this.active = active
        this.activeAgo = activeAgo
    }
}

export class Message {
    id?: string;
    roomID?: string;
    userID?: string;
    message?: string;
    timestamp?: number;
    createdAt?: string;
    room: Room;
    user: User;
    constructor(
        id: string = '',
        roomID: string = '',
        userID: string = '',
        message: string = '',
        timestamp: number = 0,
        createdAt: string = '',
        room: Room = new Room,
        user: User = new User
    ) {
        this.id = id;
        this.roomID = roomID;
        this.userID = userID;
        this.message = message;
        this.timestamp = timestamp;
        this.createdAt = createdAt;
        this.room = room;
        this.user = user;
    }
}

export class Room {
    id: string;
    name: string;
    constructor(
        id: string = '' +
        '',
        name: string = '' +
        ''
    ) {
        this.id = id;
        this.name = name;
    }
}

export type SignInType = {
    username: string;
    password: string
}

export type SignUpType = {
    username: string
    password: string
    email: string
    phone: string
}

export type SignUpConfirmationType = {
    username: string
    authenticationCode: string
}

export type ForgotPasswordType = {
    username: string
}

export type ForgotPasswordSubmitType = {
    username: string
    code: string
    password: string
}
