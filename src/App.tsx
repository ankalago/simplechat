import React from 'react';
import './App.scss';
import Amplify from 'aws-amplify'
import awsExports from "./aws-exports";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Chat from './pages/Chat';
import Login from './pages/Login';
import SignUp from './pages/SignUp';
import Forgot from './pages/Forgot';

Amplify.configure(awsExports);

const App = () => {

    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Login />
                </Route>
                <Route exact path="/signup">
                    <SignUp />
                </Route>
                <Route exact path="/forgot">
                    <Forgot />
                </Route>
                <Route exact path="/chat">
                    <Chat />
                </Route>
            </Switch>
        </Router>
    );
}

export default App
