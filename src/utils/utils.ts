export const randomNumber = (max: number = 600) =>
    Math.floor(Math.random() * max) + 1

export const getTimestamp = () => {
    const currentDate = new Date();
    return currentDate.getTime();
}
