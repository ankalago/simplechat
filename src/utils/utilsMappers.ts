import {User} from '../types/components';
import {getTimestamp, randomNumber} from './utils';

export const userMapper = (user: User): User => {
    const random = randomNumber();
    return {
        id: user.id,
        username: user.username,
        email: user.email,
        image: `https://picsum.photos/id/${random}/42/42.jpg`,
        active: true,
        activeAgo: getTimestamp(),
    }
}
