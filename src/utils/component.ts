import {notification} from 'antd';
import {
    ForgotPasswordSubmitType,
    ForgotPasswordType,
    Room,
    SignInType,
    SignUpConfirmationType,
    SignUpType,
    User
} from '../types/components';
import {getTimestamp} from './utils';
import {createMessage, createUser, updateUser} from '../graphql/mutations';
import {Auth, API, graphqlOperation} from 'aws-amplify'
import {listRooms, listUsers} from '../graphql/queries';

export const openNotification = (error: any, type: string = 'error') => {
    // @ts-ignore
    notification[type]({
        message: error.message,
    });
};

export const updateLastConnectionUser = async (user: User) => {
    const variablesUpdateUser = {
        input: {
            id: user.id,
            activeAgo: getTimestamp(),
            active: true
        },
        condition: {
            username: {
                eq: user.username
            }
        }
    }
    try {
        await API.graphql(graphqlOperation(updateUser, variablesUpdateUser))
    } catch (e) {
        console.log('error: ', e);
    }
}

export const getCurrentUser = async (callBack?: Function) => {
    let username = '';
    let email = '';
    try {
        const currentUser = await Auth.currentUserInfo()
        username = currentUser?.username
        email = currentUser?.attributes?.email
        !username && callBack && callBack()
        return {username, email}
    } catch (e) {
        console.log('error: ', e);
        callBack && callBack()
    }
    return {username, email};
}

export const getListUsersData = async () => {
    let listUsersData = [];
    try {
        const usersListData = await API.graphql(graphqlOperation(listUsers));
        // @ts-ignore
        listUsersData = usersListData?.data?.listUsers.items;
        return {listUsersData}
    } catch (e) {
        console.log('error: ', e);
    }
    return {listUsersData};
}

export const getListRoomsData = async () => {
    let listRoomsData = [];
    try {
        const roomMessageListData = await API.graphql(graphqlOperation(listRooms));
        // @ts-ignore
        listRoomsData = roomMessageListData?.data?.listRooms.items[0];
        return {listRoomsData}
    } catch (e) {
        console.log('error: ', e);
    }
    return {listRoomsData};
}

export const addUser = async (user: User) => {
    try {
        await API.graphql(graphqlOperation(createUser, {input: user}));
    } catch (e) {
        console.log('error: ', e);
    }
}

export const addMessage = async (user: User, room: Room, message: string) => {
    const variablesMessageData = {
        input: {
            message,
            roomID: room.id,
            userID: user.id,
            timestamp: getTimestamp()
        }
    }
    try {
        message.length && await updateLastConnectionUser(user)
        message.length && await API.graphql(graphqlOperation(createMessage, variablesMessageData))
    } catch (e) {
        console.log('error: ', e);
    }
}

export const signOut = async (user: User) => {
    try {
        const variablesUpdateUser = {
            input: {
                id: user.id,
                active: false
            },
            condition: {
                username: {
                    eq: user.username
                }
            }
        }
        await API.graphql(graphqlOperation(updateUser, variablesUpdateUser));
        await Auth.signOut({global: true});
    } catch (error) {
        console.log('error signing out: ', error);
    }
}

export const signIn = async (values: SignInType, callBack: Function) => {
    const {username, password} = values
    try {
        await Auth.signIn({username, password})
        callBack()
    } catch (error) {
        openNotification(error)
        console.log('***************** error: ', error);
    }
}

export const forgotPassword = async (values: ForgotPasswordType, callBack: Function) => {
    const {username} = values
    try {
        await Auth.forgotPassword(username)
        callBack()
    } catch (error) {
        openNotification(error)
        console.log('***************** error: ', error);
    }
}

export const forgotPasswordSubmit = async (values: ForgotPasswordSubmitType, callBack: Function) => {
    const {username, code, password} = values
    try {
        await Auth.forgotPasswordSubmit(username, code, password)
        callBack()
    } catch (error) {
        openNotification(error)
        console.log('***************** error: ', error);
    }
}

export const signUp = async (values: SignUpType, callBack: Function) => {
    const {username, password, email, phone} = values;
    try {
        await Auth.signUp({username, password, attributes: {email, phone_number: phone}})
        callBack()
    } catch (error) {
        openNotification(error)
        console.log('***************** error: ', error);
    }
}

export const confirmSignUp = async (values: SignUpConfirmationType, callBack: Function) => {
    const {username, authenticationCode} = values
    try {
        await Auth.confirmSignUp(username, authenticationCode)
        callBack()
    } catch (error) {
        openNotification(error)
        console.log('***************** error: ', error);
    }
}
