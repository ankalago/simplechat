/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateRoom = /* GraphQL */ `
  subscription OnCreateRoom {
    onCreateRoom {
      id
      name
      createdAt
      messages {
        items {
          id
          roomID
          userID
          message
          timestamp
          createdAt
          updatedAt
        }
        nextToken
      }
      updatedAt
    }
  }
`;
export const onUpdateRoom = /* GraphQL */ `
  subscription OnUpdateRoom {
    onUpdateRoom {
      id
      name
      createdAt
      messages {
        items {
          id
          roomID
          userID
          message
          timestamp
          createdAt
          updatedAt
        }
        nextToken
      }
      updatedAt
    }
  }
`;
export const onDeleteRoom = /* GraphQL */ `
  subscription OnDeleteRoom {
    onDeleteRoom {
      id
      name
      createdAt
      messages {
        items {
          id
          roomID
          userID
          message
          timestamp
          createdAt
          updatedAt
        }
        nextToken
      }
      updatedAt
    }
  }
`;
export const onCreateMessage = /* GraphQL */ `
  subscription OnCreateMessage {
    onCreateMessage {
      id
      roomID
      userID
      message
      timestamp
      createdAt
      room {
        id
        name
        createdAt
        messages {
          nextToken
        }
        updatedAt
      }
      user {
        id
        username
        email
        image
        active
        activeAgo
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const onUpdateMessage = /* GraphQL */ `
  subscription OnUpdateMessage {
    onUpdateMessage {
      id
      roomID
      userID
      message
      timestamp
      createdAt
      room {
        id
        name
        createdAt
        messages {
          nextToken
        }
        updatedAt
      }
      user {
        id
        username
        email
        image
        active
        activeAgo
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const onDeleteMessage = /* GraphQL */ `
  subscription OnDeleteMessage {
    onDeleteMessage {
      id
      roomID
      userID
      message
      timestamp
      createdAt
      room {
        id
        name
        createdAt
        messages {
          nextToken
        }
        updatedAt
      }
      user {
        id
        username
        email
        image
        active
        activeAgo
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser {
    onCreateUser {
      id
      username
      email
      image
      active
      activeAgo
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser {
    onUpdateUser {
      id
      username
      email
      image
      active
      activeAgo
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser {
    onDeleteUser {
      id
      username
      email
      image
      active
      activeAgo
      createdAt
      updatedAt
    }
  }
`;
