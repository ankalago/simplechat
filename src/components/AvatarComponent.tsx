import {Badge, Tooltip} from 'antd';
import React from 'react';
import {User} from '../types/components';
import { formatDistance } from 'date-fns'

type Props = {
    user: User
}

const AvatarComponent = ({user}: Props) => {
    const {active, image, username, activeAgo} = user ?? new User();
    return (
        <div className="wrapper-avatar">
            <div className="wrapper-badge">
                <Tooltip placement="bottomLeft" title={username}>
                    <Badge status={active ? 'success' : 'default'} dot={true} className="badge">
                        <a href="#" className="avatar" style={{backgroundImage: `url(${image})`}}/>
                    </Badge>
                </Tooltip>
            </div>
            <div className="wrapper-user">
                <div className="username">
                    {username}
                </div>
                <div className="timestamp">
                    {formatDistance(activeAgo || new Date(), new Date())}
                </div>
            </div>
        </div>
    )
}

export default AvatarComponent
