import React from 'react';
import {Message, User} from '../types/components';
import { format } from 'date-fns'
import {Tooltip} from 'antd';

type Props = {
    message: Message
    user: User
}

const MessageComponent = ({message, user}: Props) => {
    const {user: userInfo, message: messageInfo, timestamp } = message ?? new Message();
    return (
        <div className={`wrapper-message ${userInfo?.id === user?.id ? 'same-user' : ''}`}>
            <div className="avatar">
                <Tooltip placement="bottomLeft" title={userInfo?.username}>
                    <img src={userInfo?.image} alt={userInfo?.username}/>
                </Tooltip>
            </div>
            <div className="message">{messageInfo}</div>
            {/*<div className="date">{format(formatDistance(timestamp || new Date(), new Date()), "HH:mm:ss")}</div>*/}
            <div className="date">{format(timestamp || new Date(), "HH:mm")}</div>
        </div>
    )
}

export default MessageComponent
