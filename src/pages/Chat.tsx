import React, {useEffect, useState} from 'react';
import '../App.scss';
import Amplify, {API, graphqlOperation} from 'aws-amplify'
import awsExports from "../aws-exports";
import {Layout, Input} from 'antd';
import {PoweroffOutlined, SendOutlined} from '@ant-design/icons';
import {userMapper} from '../utils/utilsMappers';
import {Room, User} from '../types/components';
import {onCreateMessage, onUpdateUser} from '../graphql/subscriptions';
import AvatarComponent from '../components/AvatarComponent';
import MessageComponent from '../components/MessageComponent';
import {useHistory} from 'react-router';
import {
    addMessage,
    addUser,
    getCurrentUser,
    getListRoomsData,
    getListUsersData,
    openNotification, signOut,
    updateLastConnectionUser
} from '../utils/component';

Amplify.configure(awsExports);

const Chat = () => {

    const [users, setUsers] = useState([])
    const [user, setUser] = useState(new User())
    const [userUpdate, setUserUpdate] = useState(new User())
    const [room, setRoom] = useState(new Room())
    const [message, setMessage] = useState('')
    const [listMessage, setListMessage] = useState([])
    const [newMessage, setNewMessage] = useState({})
    const history = useHistory();

    const getUserInfo = async () => {
        const userLogin: User = await getCurrentUser(() => history.push('/'));
        const {listUsersData} = await getListUsersData();
        const filterUser = listUsersData.filter((user: User) => user.username === userLogin.username);
        const existUsersData = filterUser.length > 0;
        const userData = existUsersData ? filterUser[0] : userMapper(userLogin)
        !existUsersData && await addUser(userData)
        existUsersData && await updateLastConnectionUser(userData)
        setUsers(listUsersData);
        setUser(userData)
    }

    useEffect(() => {
        getUserInfo()
        subscribeRoomListMessage()
        subscribeListUsers()
        return () => {
            subscriptionRoomListMessage && subscriptionRoomListMessage.unsubscribe();
            subscriptionRoomListMessage && subscriptionListUsers.unsubscribe();
        }
    }, []);

    useEffect(() => {
        getRoomListMessages()
    }, [newMessage])

    const getRoomListMessages = async () => {
        const {listRoomsData} = await getListRoomsData();
        setRoom(listRoomsData);
        setListMessage(listRoomsData?.messages?.items)
        const conversation = document.querySelector('.container-messages');
        // @ts-ignore
        conversation && conversation.scrollTo(0, conversation.scrollHeight)
    }

    useEffect(() => {
        getUsers()
    }, [user, userUpdate])

    const getUsers = async () => {
        const {listUsersData} = await getListUsersData();
        setUsers(listUsersData);
    }

    let subscriptionRoomListMessage: any;

    const subscribeRoomListMessage = async () => {
        try {
            const {username} = await getCurrentUser();
            // @ts-ignore
            subscriptionRoomListMessage = await API.graphql(graphqlOperation(onCreateMessage)).subscribe({
                next: ({provider, value}: any) => {
                    console.log('onCreateMessage: ', value);
                    console.log('user: ', username);
                    const messageCreated = value?.data?.onCreateMessage
                    const error = { message: `You did you receive a new message of ${messageCreated.user.username}`};
                    username !== messageCreated.user.username && openNotification( error, 'success')
                    setNewMessage(value?.data?.onCreateMessage)
                }
            });
        } catch (e) {
            console.log('error: ', e);
        }
    }

    let subscriptionListUsers: any;

    const subscribeListUsers = async () => {
        try {
            const {username} = await getCurrentUser();
            // @ts-ignore
            subscriptionListUsers = await API.graphql(graphqlOperation(onUpdateUser)).subscribe({
                next: ({provider, value}: any) => {
                    console.log('onUpdateUser: ', value);
                    console.log('user: ', username);
                    const userUpdate = value?.data?.onUpdateUser
                    username === userUpdate.username && setUser(value?.data?.onUpdateUser)
                    username !== userUpdate.username && setUserUpdate(userUpdate)
                }
            });
        } catch (e) {
            console.log('error: ', e);
        }
    }

    const handleSignOut = async () => {
        await signOut(user)
        history.push('/')
    }

    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value
        value.length <= 160 && setMessage(value)
    }

    const handleAddMessage = async () => {
        await addMessage(user, room, message)
        setMessage('')
    }

    const {Header, Footer, Sider, Content} = Layout;

    return (
        <div className="App">
            <Layout className="layout">
                <Sider width={250} className='sider' breakpoint={'xs'} collapsedWidth="0" >
                    <div className="wrapper-users">
                        {users.map((item, index) =>
                            <AvatarComponent key={index} user={item}/>
                        )}
                    </div>
                </Sider>
                <Layout>
                    <Header className="header">
                        <div className="wrapper-information">
                            <div className="wrapper-avatar">
                                <AvatarComponent user={user}/>
                            </div>
                        </div>
                        <div className="sing-out">
                            <PoweroffOutlined onClick={handleSignOut} className="hydrated"/>
                        </div>
                    </Header>
                    <Content>
                        <div className="container-messages" id="container-messages">
                            <div className="fixed"/>
                            {listMessage?.length ? listMessage.map((item, index) => (
                                <MessageComponent message={item} key={index} user={user}/>
                            )) : null}
                        </div>
                    </Content>
                    <Footer>
                        <div className="wrapper-message">
                            <Input placeholder="write your message" bordered={false} onChange={handleInput}
                                   onPressEnter={handleAddMessage} maxLength={160} value={message}/>
                            <SendOutlined className="send-message" onClick={handleAddMessage}/>
                        </div>
                    </Footer>
                </Layout>
            </Layout>
        </div>
    );
}

export default Chat
