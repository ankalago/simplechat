import React, {useEffect, useState} from 'react';
import '../Login.scss';
import {Auth} from 'aws-amplify'
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined, PhoneOutlined, KeyOutlined } from '@ant-design/icons';
import {useHistory} from 'react-router';
import { Link } from 'react-router-dom';
import {confirmSignUp, signUp} from '../utils/component';
import {SignUpConfirmationType, SignUpType} from '../types/components';

const SignUp = () => {

    const history = useHistory();
    const [step, setStep] = useState(0)

    const handleSignUp = async (values: SignUpType) => {
        await signUp(values, () => setStep(1))
    }

    const handleConfirmSignUp = async (values: SignUpConfirmationType) => {
        await confirmSignUp(values, () => history.push('/'))
    }

    const validateLogin = async () => {
        const currentUser = await Auth.currentUserInfo()
        currentUser?.username && history.push('/chat')
    }

    useEffect(() => {
        validateLogin()
    }, [])

    return (
        <div className="Login">
            <div className="login-warp">
                {step === 0 && (
                    <>
                        <div className="subTitle">Fill the all fields</div>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={handleSignUp}
                            size="large"
                        >
                            <Form.Item
                                name="username"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[{ required: true, message: 'Please input your Password!' }]}
                            >
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder="Password"
                                    minLength={6}
                                />
                            </Form.Item>
                            <Form.Item
                                name="email"
                                rules={[{ required: true, message: 'Please input your Email!' }]}
                            >
                                <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                            </Form.Item>
                            <Form.Item
                                name="phone"
                                rules={[{ required: true, message: 'Please input your Phone!' }]}
                            >
                                <Input prefix={<PhoneOutlined className="site-form-item-icon" />} placeholder="+593998342013" />
                            </Form.Item>

                            <Form.Item>
                                <Link to="/">Sign In</Link> |
                                <Button htmlType="submit" className="login-form-button">
                                    Sign Up
                                </Button>
                            </Form.Item>
                        </Form>
                    </>
                )}
                {step === 1 && (
                    <>
                        <div className="subTitle">Put the username and code that we send to your email</div>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={handleConfirmSignUp}
                            size="large"
                        >
                            <Form.Item
                                name="username"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                            </Form.Item>
                            <Form.Item
                                name="authenticationCode"
                                rules={[{ required: true, message: 'Please input your Authentication Code!' }]}
                            >
                                <Input prefix={<KeyOutlined className="site-form-item-icon" />} placeholder="Authentication Code" />
                            </Form.Item>
                            <Form.Item>
                                <Button htmlType="submit" className="login-form-button">
                                    Sign Up
                                </Button>
                            </Form.Item>
                        </Form>
                    </>
                )}
            </div>
        </div>
    )
}

export default SignUp
