import React, {useEffect, useState} from 'react';
import '../Login.scss';
import { Form, Input, Button, Divider } from 'antd';
import {KeyOutlined, UserOutlined} from '@ant-design/icons';
import {useHistory} from 'react-router';
import { Link } from 'react-router-dom';
import {forgotPassword, forgotPasswordSubmit, getCurrentUser} from '../utils/component';
import {ForgotPasswordSubmitType, ForgotPasswordType} from '../types/components';

const Forgot = () => {

    const history = useHistory();
    const [step, setStep] = useState(0)

    const handleForgotPassword = async (values: ForgotPasswordType) => {
        await forgotPassword(values, () => setStep(1))
    }

    const handleForgotPasswordSubmit = async (values: ForgotPasswordSubmitType) => {
        await forgotPasswordSubmit(values, () => history.push('/'))
    }

    const validateLogin = async () => {
        await getCurrentUser(() => history.push('/chat'));
    }

    useEffect(() => {
        validateLogin()
    }, [])

    return (
        <div className="Login">
            <div className="login-warp">
                {step === 0 && (
                    <>
                        <div className="subTitle">Put your username</div>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={handleForgotPassword}
                            size="large"
                        >
                            <Form.Item
                                name="username"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                            </Form.Item>

                            <Form.Item>
                                <Link to="/">Sing In</Link> |
                                <Button htmlType="submit" className="login-form-button">
                                    Recovery
                                </Button>
                            </Form.Item>
                        </Form>
                    </>
                )}
                {step === 1 && (
                    <>
                        <div className="subTitle">Put your username the code sent to your email and your new password</div>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={handleForgotPasswordSubmit}
                            size="large"
                        >
                            <Form.Item
                                name="username"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                            </Form.Item>
                            <Form.Item
                                name="code"
                                rules={[{ required: true, message: 'Please input your Code!' }]}
                            >
                                <Input prefix={<KeyOutlined className="site-form-item-icon" />} placeholder="Code" />
                            </Form.Item>

                            <Divider />

                            <Form.Item
                                name="password"
                                rules={[{ required: true, message: 'Please input your New Password!' }]}
                            >
                                <Input prefix={<KeyOutlined className="site-form-item-icon" />}
                                       type="password"
                                       placeholder="New Password"
                                       minLength={6}
                                />
                            </Form.Item>

                            <Divider />

                            <Form.Item>
                                <Button htmlType="submit" className="login-form-button">
                                    Send New Password
                                </Button>
                            </Form.Item>
                        </Form>
                    </>
                )}
            </div>
        </div>
    )
}

export default Forgot
