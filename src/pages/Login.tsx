import React, {useEffect} from 'react';
import '../Login.scss';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import {useHistory} from 'react-router';
import { Link } from 'react-router-dom';
import {getCurrentUser, signIn} from '../utils/component';
import {SignInType} from '../types/components';

const Login = () => {

    const history = useHistory();

    const handleSignIn = async (values: SignInType) => {
        await signIn(values, () => history.push('/chat'));
    }

    const validateLogin = async () => {
        const {username} = await getCurrentUser();
        username && history.push('/chat')
    }

    useEffect(() => {
        validateLogin()
    }, [])

    return (
        <div className="Login">
            <div className="login-warp">
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={handleSignIn}
                    size="large"
                >
                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Please input your Username!' }]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Password"
                            minLength={6}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Link className="login-form-forgot" to="/forgot">Forgot password</Link>
                    </Form.Item>

                    <Form.Item>
                        <Link to="/signup">Register now!</Link> Or
                        <Button htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default Login
