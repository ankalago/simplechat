import {useEffect, useState} from 'react'

const useFetch = (url: string) => {
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState({})

    const getFetch = async () => {
        const response = await fetch(url)
        const data = await response.json();
        setLoading(false)
        setData(data)
    }

    useEffect(() => {
        getFetch()
    }, [])

    return {loading, data}
}

export default useFetch
